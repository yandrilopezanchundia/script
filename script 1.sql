-- -----------------------------------------------------
-- Schema sistema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sistema` DEFAULT CHARACTER SET utf8 ;
USE `sistema` ;

-- -----------------------------------------------------
-- Table `sistema`.`Departamentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistema`.`Departamentos` (
  `idDepartamentos` INT NOT NULL,
  `NombresDepartamentos` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idDepartamentos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema`.`Empleados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistema`.`Empleados` (
  `idEmpleados` INT NOT NULL,
  `Nombres` VARCHAR(45) NOT NULL,
  `Apellidos` VARCHAR(45) NOT NULL,
  `Direccion` VARCHAR(45) NULL,
  `Telefono` INT NULL,
  `CuentaBancaria` INT NULL,
  `Departamentos_idDepartamentos` INT NOT NULL,
  `DNI` INT NULL,
  PRIMARY KEY (`idEmpleados`),
  INDEX `fk_Empleados_Departamentos_idx` (`Departamentos_idDepartamentos` ASC),
  CONSTRAINT `fk_Empleados_Departamentos`
    FOREIGN KEY (`Departamentos_idDepartamentos`)
    REFERENCES `sistema`.`Departamentos` (`idDepartamentos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema`.`Categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistema`.`Categoria` (
  `idCategoria` INT NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  `Fecha_inicio` DATE NOT NULL,
  `Fecha_fin` DATE NOT NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema`.`Contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistema`.`Contrato` (
  `idContrato` INT NOT NULL,
  `Fecha_inicio` DATE NULL,
  `Fecha_fin` DATE NULL,
  `Empleados_idEmpleados` INT NOT NULL,
  `Categoria_idCategoria` INT NOT NULL,
  PRIMARY KEY (`idContrato`),
  INDEX `fk_Contrato_Empleados1_idx` (`Empleados_idEmpleados` ASC),
  INDEX `fk_Contrato_Categoria1_idx` (`Categoria_idCategoria` ASC),
  CONSTRAINT `fk_Contrato_Empleados1`
    FOREIGN KEY (`Empleados_idEmpleados`)
    REFERENCES `sistema`.`Empleados` (`idEmpleados`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Contrato_Categoria1`
    FOREIGN KEY (`Categoria_idCategoria`)
    REFERENCES `sistema`.`Categoria` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema`.`Nomina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistema`.`Nomina` (
  `idNomina` INT NOT NULL AUTO_INCREMENT,
  `Fecha_pago` DATE NULL,
  `Cantidad_ingresada` DOUBLE NOT NULL,
  `Empleados_idEmpleados` INT NOT NULL,
  PRIMARY KEY (`idNomina`),
  INDEX `fk_Nomina_Empleados1_idx` (`Empleados_idEmpleados` ASC),
  CONSTRAINT `fk_Nomina_Empleados1`
    FOREIGN KEY (`Empleados_idEmpleados`)
    REFERENCES `sistema`.`Empleados` (`idEmpleados`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Insert en la table empleados
-- -----------------------------------------------------
INSERT INTO `sistema`.`empleados`
(`idEmpleados`,
`Nombres`,
`Apellidos`,
`Direccion`,
`Telefono`,
`CuentaBancaria`,
`Departamentos_idDepartamentos`,
`DNI`)
VALUES
(1,
"Yandri",
"Lopez",
"Barri0 Jocay",
0986537377,
1154818182,
20183,
1313838276);
-- -----------------------------------------------------
-- Insert en la table departamentos
-- -----------------------------------------------------
INSERT INTO `sistema`.`departamentos`
(`idDepartamentos`,
`NombresDepartamentos`)
VALUES
(20184,
"Recursos Humanos");
-- -----------------------------------------------------
-- Insert en la table categoria
-- -----------------------------------------------------
INSERT INTO `sistema`.`categoria`
(`idCategoria`,
`Nombre`,
`Fecha_inicio`,
`Fecha_fin`)
VALUES
(16201,
"Administrativo Nivel 1",
'2018-11-17',
'2019-11-17');
-- -----------------------------------------------------
-- Modify en columna cantidad ingresada utilizando un ALTER TABLE
-- -----------------------------------------------------
use sistema;
ALTER TABLE nomina MODIFY Cantidad_ingresada JSON NOT NULL;
-- -----------------------------------------------------
-- Insertar ahora en formato de JSON
-- -----------------------------------------------------
INSERT INTO nomina
(`idNomina`,
`Fecha_pago`,
`Cantidad_ingresada`, 
`Empleados_idEmpleados`)
VALUES
(2,
'2018-12-30',
'{"pago":{"codigoBanco":"0001", 
"nombreBanco":"Pacifico", 
"cuentaTipo":"Ahorro", "CuentaNumero":"1154787182", 
"DetalleValores":[{"Sueldo":"1000"}, {"Extras":"100"} ] } }',
2);
-- -----------------------------------------------------
-- Consultar por medio de id a categoria,departamentos,empleados
-- -----------------------------------------------------

select empleados.idEmpleados, categoria.idCategoria,
departamentos.idDepartamentos, empleados.Nombres, 
categoria.Nombre, departamentos.NombresDepartamentos 
from contrato
inner join empleados 
on empleados.idEmpleados = contrato.Empleados_idEmpleados
inner join categoria 
on categoria.idCategoria = contrato.Categoria_idCategoria
inner join departamentos
on departamentos.idDepartamentos = empleados.Departamentos_idDepartamentos
