
use sistema;
-- -----------------------------------------------------
-- Crear los usuarios con sus permisos en Aministradores
-- -----------------------------------------------------
CREATE USER 'Administrador1'@'localhost' IDENTIFIED BY 'admin1';
GRANT ALL ON sistema.* TO 'Administrador1'@'localhost';

CREATE USER 'Administrador2'@'localhost' IDENTIFIED BY 'admin2';
GRANT ALL ON sistema.* TO 'Administrador2'@'localhost';
-- -----------------------------------------------------
-- Crear los usuarios con sus permisos en Gerentes
-- -----------------------------------------------------

CREATE USER 'Gerente1'@'%' IDENTIFIED BY 'gen1';
GRANT SELECT ON sistema.* TO 'Gerente1'@'%';

CREATE USER 'Gerente2'@'%' IDENTIFIED BY 'gen2';
GRANT SELECT ON sistema.* TO 'Gerente2'@'%';
-- -----------------------------------------------------
-- Crear los usuarios con sus permisos en Contables
-- -----------------------------------------------------

CREATE USER 'Contables1'@'%' IDENTIFIED BY 'con1';
GRANT SELECT ON sistema.* TO 'Contables1'@'%';
GRANT ALTER ON sistema.nomina TO 'Contables1'@'%';

CREATE USER 'Contables2'@'%' IDENTIFIED BY 'con2';
GRANT SELECT ON sistema.* TO 'Contables2'@'%';
GRANT ALTER ON sistema.nomina TO 'Contables2'@'%';

-- -----------------------------------------------------
-- Crear los usuarios con sus permisos en Operadores
-- -----------------------------------------------------

CREATE USER 'Operador1'@'localhost' IDENTIFIED BY 'ope1';
GRANT SELECT,INSERT,DELETE,UPDATE ON sistema.departamentos TO 'Operador1'@'%';
GRANT SELECT,INSERT,DELETE,UPDATE ON sistema.categoria TO 'Operador1'@'%';
GRANT SELECT,INSERT,DELETE,UPDATE ON sistema.empleados TO 'Operador1'@'%';

CREATE USER 'Operador2'@'localhost' IDENTIFIED BY 'ope2';
GRANT SELECT,INSERT,DELETE,UPDATE ON sistema.departamentos TO 'Operador2'@'%';
GRANT SELECT,INSERT,DELETE,UPDATE ON sistema.categoria TO 'Operador2'@'%';
GRANT SELECT,INSERT,DELETE,UPDATE ON sistema.empleados TO 'Operador2'@'%';

